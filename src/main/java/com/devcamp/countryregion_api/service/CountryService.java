package com.devcamp.countryregion_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregion_api.model.Country;

@Service
public class CountryService {
    private static ArrayList<Country> countryList = new ArrayList<Country>();

    static {
        new RegionService();
        Country country1 = new Country("C01", "Viet Nam", RegionService.getListRegion1());
        Country country2 = new Country("C02", "VIET NAM", RegionService.getListRegion2());
        Country country3 = new Country("C03", "viet nam", RegionService.getListRegion3());      
        countryList.add(country1);
        countryList.add(country2);
        countryList.add(country3);  
    }

    public ArrayList<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(ArrayList<Country> countryList) {
        CountryService.countryList = countryList;
    }
}
