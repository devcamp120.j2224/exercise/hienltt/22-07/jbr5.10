package com.devcamp.countryregion_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.countryregion_api.model.Region;
@Service
public class RegionService {
    private static ArrayList<Region> listRegion1 = new ArrayList<Region>();
    private static ArrayList<Region> listRegion2 = new ArrayList<Region>();
    private static ArrayList<Region> listRegion3 = new ArrayList<Region>();
    private static ArrayList<Region> listAllRegion = new ArrayList<Region>();

    static {
        Region region1 = new Region("A001", "TP.HCM");
        Region region2 = new Region("B002", "TIEN GIANG");
        listRegion1.add(region1);
        listRegion1.add(region2);
        Region region3 = new Region("C003", "CAN THO");
        Region region4 = new Region("D004", "BEN TRE");
        listRegion2.add(region3);
        listRegion2.add(region4);
        Region region5 = new Region("E005", "VUNG TAU");
        Region region6 = new Region("F006", "BINH DUONG");
        listRegion3.add(region5);
        listRegion3.add(region6);
        listAllRegion.add(region1);
        listAllRegion.add(region2);
        listAllRegion.add(region3);
        listAllRegion.add(region4);
        listAllRegion.add(region5);
        listAllRegion.add(region6); 
    }

    public  ArrayList<Region> getListAllRegion() {
        return listAllRegion;
    }

    public  void setListAllRegion(ArrayList<Region> listAllRegion) {
        RegionService.listAllRegion = listAllRegion;
    }

    public static ArrayList<Region> getListRegion1() {
        return listRegion1;
    }

    public void setListRegion1(ArrayList<Region> listRegion1) {
        RegionService.listRegion1 = listRegion1;
    }

    public static ArrayList<Region> getListRegion2() {
        return listRegion2;
    }

    public void setListRegion2(ArrayList<Region> listRegion2) {
        RegionService.listRegion2 = listRegion2;
    }

    public static ArrayList<Region> getListRegion3() {
        return listRegion3;
    }

    public void setListRegion3(ArrayList<Region> listRegion3) {
        RegionService.listRegion3 = listRegion3;
    }
}
