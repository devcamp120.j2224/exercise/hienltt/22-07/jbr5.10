package com.devcamp.countryregion_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregion_api.model.Country;
import com.devcamp.countryregion_api.service.CountryService;

@RestController
public class CountryController {
    @Autowired
    private CountryService countryService;
    
    @CrossOrigin
    @GetMapping("/countries")
    public ArrayList<Country> getCountryList(){
        ArrayList<Country> allCountry = countryService.getCountryList();
        return allCountry;
    }

    @CrossOrigin
    @GetMapping("/country-info")
    public ArrayList<Country> getCountryInfo(@RequestParam(required =  true) String countryCode){
        ArrayList<Country> allCountry = countryService.getCountryList();
        ArrayList<Country> result = new ArrayList<>();
        for (int i = 0; i < allCountry.size(); i++){
            if(allCountry.get(i).getCountryCode().equals(countryCode)){
                result.add(allCountry.get(i));
            }   
        }
        return result;
    }
}
