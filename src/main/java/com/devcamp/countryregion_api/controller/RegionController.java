package com.devcamp.countryregion_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.countryregion_api.model.Region;
import com.devcamp.countryregion_api.service.RegionService;

@RestController
public class RegionController {
    @Autowired
    private RegionService regionService;

    @CrossOrigin
    @GetMapping("/region-info")
    public ArrayList<Region> getCountryList(@RequestParam(required =  true) String regionCode ){
        ArrayList<Region> allRegion = regionService.getListAllRegion();
        ArrayList<Region> result = new ArrayList<>();
        for (int i = 0; i < allRegion.size(); i++){
            if(allRegion.get(i).getRegionCode().equals(regionCode)){
                result.add(allRegion.get(i));
            }   
        }
        return result;
    }
}
